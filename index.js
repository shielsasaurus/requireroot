requireFromRoot = (function(root) { // This function allows us to 'require' from the app's root directory
    return function(resource) {
        return require(root+"/"+resource);
    }
})(__dirname);

module.exports = requireFromRoot;